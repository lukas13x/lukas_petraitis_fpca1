--Lukas Petraitis D00163815
--Functional Programming CA1
--Q1 Adds two numbers together and doubles the result
addAndDouble :: (Num a) => a -> a -> a
addAndDouble a b = (a + b) * 2
--Q2 Infix operator that does the same as addAndDouble
(+*) :: (Num a) => a -> a -> a
(+*) a b = a `addAndDouble` b
--Q3 solves a quadraticEquation, takes in three arguments (a,b,c). Source: stackoverflow
quadraticEquation :: (Double,Double,Double) -> (Double, Double)
quadraticEquation (a, b, c) = (x, y)
     where
         x = e + sqrt d / (2 * a)
         y = e - sqrt d / (2 * a)
         d = b * b - 4 * a * c
         e = - b / (2 * a)
--Q4 Takes an Int value n and returns a list of first n Ints from an infinite list. 
-- Credit to Adams for helping to implement the take function
first_n :: Int -> [Int] -> [Int]
first_n  n (x:xs)= x: take (n-1) xs 
--Q5
--first_n_integers
--Q6 takes an integer and computes the product of all factorials 
double_factorial :: Integer -> [Integer]
double_factorial 0 = [1] 
double_factorial x = [factorial x] ++ double_factorial (x-1)
     where
      factorial 0 = 1
      factorial n = n * factorial (n-1)
--Q7 provides an infinite list of factorials, use take function to select how many factorial products to view
factorials :: [Integer]
factorials = 1 : zipWith (*) factorials [1..]
--Q8 Tells you if the number is a prime or not. Source: stackoverflow
isPrime :: Integer->Bool
isPrime x = if x > 2
     then ((x `mod` (x-1)) /=0) && not (isPrime (x-1))
     else False
--Q9 Returns all primes numbers from an entered list. 
primes :: [Integer]
primes = filter isPrime [1..]
--Q10 Sums a list of integers recursively,returns 0 if its an empty list
sum' ::[Int]->Int
sum' [] = 0
sum' (x:xs) = sum(x:xs)
--Q11 Sums a list of integers in terms of foldl meaning it goes through the list from left to right, 
--retunrs 0 if its an empty list
foldlSum :: [Int] -> Int
foldlSum [] = 0
foldlSum (x:xs) = foldl (+) x xs 
--Q12 Multiplies a list of integers in terms of foldr, meaning it multiplies each element in the list
--going from right to left and returns 0 if its an empty list
foldrMultiply :: [Int] -> Int
foldrMultiply [] = 0
foldrMultiply (x:xs) = foldr (*) x xs
--Q13
--guess :: String -> a -> String
--Q14 gets the dot product of any two vectors of any length
dotProduct :: (Num a) => [a] -> [a] -> a
dotProduct [] _ = 0
dotProduct _ [] = 0
dotProduct (x:xs) (y:ys) = (x * y) + dotProduct xs ys
--Q15 Check if a number is divisble by 2 and returns true
is_even :: Int -> Bool 
is_even n = n `mod` 2 == 0
--Q16 - fiters through the user input checking if there are vowels using the infix operator notElem
unixname :: String -> String
unixname = filter (`notElem` "aeiouAEIOU")
--Q17
--intersection :: [a] -> [a] -> [a] 
--Q18
--censor :: [Char] -> [Char]